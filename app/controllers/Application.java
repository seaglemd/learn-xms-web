package controllers;

import play.*;
import play.data.DynamicForm;
import play.mvc.*;

import views.html.*;

import static play.data.Form.form;

public class Application extends Controller {

  public Result index() {
    String systemMessage="This is a follow allong of the XMS system where I learn how the services work.";
    return ok(index.render(systemMessage));
  }

  public Result login() {
    DynamicForm bindedForm = form().bindFromRequest();
    String username = bindedForm.get("username");
    return username.length() > 0 ? ok(login.render(username)) : index();
  }
}

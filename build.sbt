name := """LearnXMS"""
organization := "MereSecure"

version := "0.0.1-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, RpmPlugin, RpmDeployPlugin)

scalaVersion := "2.11.11"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs
)

publishMavenStyle := true

credentials += Credentials(Path.userHome / ".sbt" / ".credentials")

crossPaths := false

publishTo := {
  val nexus = "http://nexus.mindera/"
  if (isSnapshot.value)
    Some("snapshots" at nexus + "content/repositories/snapshots")
  else
    Some("releases" at nexus + "content/repositories/releases")
}

publishTo in Rpm := {
  val nexus = "http://nexus.mindera/"
  Some("yum_releases" at nexus + "content/repositories/yum_releases")
}

routesGenerator := InjectedRoutesGenerator

fork in run := true

//RPM package config
javaOptions in Universal ++= Seq(
  "-J-Xmx1024m",
  "-J-Xms512m",
  s"-Dpidfile.path=/var/run/${packageName.value}/play.pid",
  s"-Dconfig.file=/usr/share/${packageName.value}/conf/application.conf",
  s"-Dlogger.file=/usr/share/${packageName.value}/conf/logger.xml"
)

maintainer in Linux := "Matthew Seagle <matthew.seagle@meresecure.com>"

packageSummary in Linux := "MereSecure Exchange Follow Along"

packageDescription in Linux := "MereSecure Exchange Follow Along"

version in Rpm <<= sbtVersion apply { sv => sv split "[^\\d]" filterNot (_.isEmpty) mkString "."}

rpmRelease := sys.props.getOrElse("BUILD_NUMBER", default = "1")

rpmVendor := "meresecure"

rpmUrl := Some("http://www.meresecure.com")

rpmLicense := Some("All rights reserved")

rpmGroup := Some("meresecure")
